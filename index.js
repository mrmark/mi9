'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var mainCtrl = require('./controller/main');
var app = express();
var PORT = process.env.PORT || 4000;

app.use(bodyParser.json());

app.post('/', function(req, res, next) {
  res.json({response: mainCtrl.filter(req.body)});
});

app.use(function(err, req, res, next) {
  res.status(400).json({error: 'Could not decode request: JSON parsing failed'});
});

app.listen(PORT, function() {
  console.log('App is listening on port ' + PORT + '!');
});

