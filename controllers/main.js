'use strict';

var _ = require('lodash');

module.exports = {

  filter: function(data) {
    if (!data || !_.isArray(data.payload)) {
      return {};
    }

    return _.map(_.filter(data.payload, function(item) {
      return item.drm === true && item.episodeCount > 0;
    }), function(item) {
      return {
        image: item.image && item.image.showImage,
        slug: item.slug,
        title: item.title
      };
    });
  }

};
