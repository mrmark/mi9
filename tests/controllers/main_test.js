'use strict';

var mainCtrl = require('../../controllers/main');
var expect = require('chai').expect;

describe('MainController', function() {
  describe('filter method', function () {
    it('should return empty object if data is undefined', function() {
      expect(mainCtrl.filter(undefined)).to.be.empty;
    });

    it('should return empty object if data.payload is undefined', function() {
      expect(mainCtrl.filter({})).to.be.empty;
    });

    it('should return filtered response correctly', function() {
      var data = {
        payload: [{
          "drm": true,
          "episodeCount": 3,
          "image": {
            "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
          },
          "slug": "show/16kidsandcounting",
          "title": "16 Kids and Counting",
        }, {
          "slug": "show/seapatrol",
          "title": "Sea Patrol",
          "tvChannel": "Channel 9"
        }, {
          "drm": true,
          "episodeCount": 0,
          "image": {
            "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/TwoandahHalfMen_V2.jpg"
          },
          "slug": "show/twoandahalfmen",
          "title": "Two and a Half Men"
        }, {
          "drm": false,
          "episodeCount": 3,
          "image": {
            "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/AGT.jpg"
          },
          "slug": "show/australiasgottalent",
          "title": "Australia's Got Talent"
        }]
      };

      expect(mainCtrl.filter(data)).to.eql([{
        "image": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg",
        "slug": "show/16kidsandcounting",
        "title": "16 Kids and Counting"
      }]);

    });
  });
});
